var bannerTable;
$(function () {
    var bannerTableId = $('#bannerTable'), isReviewed = $('#isReviewed');
    bannerTable = bannerTableId.DataTable({
        ajax: {
            url: bannerUrlJsonData
        },
        processing: true,
        serverSide: true,
        order: [[4, "desc"]],
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            {data: 'url_path', title: 'Đường dẫn', name: 'url_path', sortable:false, searchable: false, render(data, type, row) {
                    return `<a class="btn btn-block btn-lg bg-pink waves-effect" href='${data}'>${data}</a>`;
                }
            },
            {data: 'banner_path', title: 'Khoá học', name: 'banner_path', sortable:false, searchable: false, render: function(data, type, row) {
                    return `<img class="img-responsive thumbnail" src="/${data}"/>`;
                }
            },
            {data: 'created_at', title: 'Ngày tạo', name: 'created_at'},
            {data: 'order', title: 'Thứ tự', name: 'order'},
            {
                data: 'id', title: 'Thao tác ', name: 'id', sortable: false, searchable: false,
                render: function (data, type, row, meta) {
                    return '<div class="icon-button-demo">' +
                        '<a href=' + bannerUrlPage + '/' + data + '/edit' + ' type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons">edit</i></a>' +
                        '<button id="deleteBanner" onclick="javascript: deleteBanner(' + data + ')" type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float"><i class="material-icons">delete</i></button></div>';
                }
            },
        ]
    });


});
var deleteBanner = function(bannerId) {
    deleteItem(bannerUrlPage + '/' + bannerId, bannerTable);
}

