﻿$(function () {
    $('#endDate input').datepicker({
        autoclose: true,
        container: '#endDate'
    });
    $('#startDate input').datepicker({
        autoclose: true,
        container: '#startDate'
    });

    //Widgets count
    $('.count-to').countTo();

    //Sales count to
    $('.sales-count-to').countTo({
        formatter: function (value, options) {
            return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, ' ').replace('.', ',');
        }
    });

    initSparkline();
});


function initSparkline() {
    $(".sparkline").each(function () {
        var $this = $(this);
        $this.sparkline('html', $this.data());
    });
}


let chartConfig = {
    type: 'area',
    stacked: true,
    title: {
        text: 'Thống kê thu nhập theo tháng',
        fontColor: '#4caee7'
    },
    subtitle: {
        text: 'Mệnh giá VNĐ ',
        fontColor: '#cb8670'
    },
    plot: {
        activeArea: true,
        animation: {
            delay: 50,
            effect: 'ANIMATION_SLIDE_LEFT',
            method: 'ANIMATION_REGULAR_EASE_OUT',
            sequence: 'ANIMATION_NO_SEQUENCE',
            speed: 1500
        },
        hoverMarker: {
            size: '10px'
        },
        tooltip: {
            text: "Tổng thu nhập tháng %kt là %vt VND."
        },
        // 'value-box': {
        //     short: 'dsds'
        // }
    },
    plotarea: {
        backgroundColor: '#fff'
    },
    scaleX: {
        lineColor: '#333',
        tick: {
            lineColor: '#333'
        },
        labels: labelX,
        label:{
            text: "Thời gian",
            fontColor: "#616161"
        }
    },
    scaleY: {
        label:{
            text: "Thu nhập",
            fontColor: "#616161"
        },
        short: true,
        item: {
            padding: '4px'
        },
        lineColor: '#333',
        tick: {
            lineColor: '#333'
        }
    },
    series: [
        {
            values: labelY,
            backgroundColor: '#4caee7',
            lineColor: '#4caee7',
            marker: {
                backgroundColor: '#4caee7',
                borderColor: '#fff',
                borderWidth: '2px'
            }
        }
    ]
};

zingchart.render({
    id: 'myChart',
    data: chartConfig
});

