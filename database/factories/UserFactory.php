<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $roles = [NORMAL, ADMIN, TEACHER, SALE];
    $statuses = [IS_ACTIVE, IS_INACTIVE];
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone_number' => '+84989454902',
        'role_id' => $roles[array_rand($roles)],
        'description' => $faker->text,
        'advisory' => $faker->text,
        'is_active' => $statuses[array_rand($statuses)],
        'avatar' => $faker->imageUrl(150, 150),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'rating' => random_int(1, 4)
    ];
});
