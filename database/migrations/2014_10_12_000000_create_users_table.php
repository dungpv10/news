<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->tinyInteger('role_id')->default(NORMAL);
            $table->string('avatar')->default('avatar.png');
            $table->string('password');
            $table->string('phone_number');
            $table->text('description')->nullable();
            $table->text('advisory')->nullable();
            $table->tinyInteger('is_active')->default(IS_ACTIVE);
            $table->string('facebook_id')->nullable();
            $table->string('google_id')->nullable();
            $table->float('rating')->default(0); // Rating for calculate income
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
