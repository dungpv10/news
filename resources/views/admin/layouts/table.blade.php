@extends('admin.layouts.layouts')
@section('css')
    <link href="/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/admin/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    @yield('table_css')
@stop

@section('content')
    @yield('table')
@stop
@section('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    @yield('javascript')
@stop
