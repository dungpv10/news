<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">ADMIN NAVIGATION</li>
        <li @if(\Request::is('education')) class="active" @endif>
            <a href="{{ route('admin.dashboard') }}">
                <i class="material-icons">home</i>
                <span>Dashboard</span>
            </a>
        </li>


        <li class="header">HỆ THỐNG</li>
        <li @if(Request::is('education/users') || Request::is('education/users/*')) class="active" @endif>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">group</i>
                <span>Thành viên</span>
            </a>
            <ul class="ml-menu">
                <li @if(\Request::route()->getName() === 'users.index') class="active" @endif>
                    <a href="{{ route('users.index') }}">Danh sách</a>
                </li>
                <li @if(\Request::route()->getName() === 'users.create') class="active" @endif>
                    <a href="{{ route('users.create') }}">
                        Tạo mới
                    </a>
                </li>
            </ul>
        </li>


    </ul>
</div>
<!-- #Menu -->
