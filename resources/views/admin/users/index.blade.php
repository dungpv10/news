@extends('admin.layouts.table')


@section('table')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Quản lý thành viên
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="card">
                    <div class="header">
                        <h2>
                            Danh sách thành viên
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('role_id', array_replace(['' => 'Nhóm quản trị'],config('role')), '', ['class' => 'form-control show-tick', 'id' => 'roleId']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('is_active', array_replace(['' => 'Trạng thái'],config('user.status')), '', ['class' => 'form-control show-tick', 'id' => 'isActive']) !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                        @include('admin.includes.success')
                        <div class="table-responsive">

                            <table id="userTable"
                                   class="table table-bordered table-striped table-hover js-basic-example dataTable"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>
@stop

@section('javascript')
    <script>
        var userUrlJsonData = "{{route('admin.users.as.json')}}";
        var userUrlPage = "{{route('users.index')}}";
    </script>
    <script src="/admin/pages/users/index.js"></script>
@stop
