@extends('admin.layouts.layouts')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Quản lý thành viên
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Tạo mới thành viên
                        </h2>
                    </div>
                    <div class="body">
                        @include('admin.includes.errors')

                        {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'id' => 'editUser']) !!}
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('name', '', [
                                            'class' => 'form-control',
                                            'required' => true,
                                            'minlength' => 2,
                                            'maxlength' => 100
                                         ])!!}
                                        <label class="form-label">Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::email('email', '', ['class' => 'form-control',
                                            'minlength' => 5,
                                            'maxlength' => 100
                                        ]) !!}
                                        <label class="form-label">Email</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::number('phone_number', '', ['class' => 'form-control',
                                            'minlength' => 10,
                                            'maxlength' => 11,
                                            'required' => true
                                        ]) !!}
                                        <label class="form-label">Số điện thoại</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'minlength' => 5, 'maxlength' => 100, 'required' => true]) !!}
                                        <label class="form-label">Mật khẩu</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'minlength' => 5, 'maxlength' => 100, 'required' => true]) !!}
                                        <label class="form-label">Xác nhận mật khẩu</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('role_id', array_replace(['' => 'Nhóm quản trị'],config('role')), '', ['class' => 'form-control show-tick', 'id' => 'roleId', 'required' => true]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('is_active', array_replace(['' => 'Trạng thái'],config('user.status')), '', ['class' => 'form-control show-tick', 'id' => 'isActive', 'required' => true]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::textarea('description', null, ['id' => 'tinymce', 'required' => true]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                {!! Form::submit('Cập nhật', ['class' => 'btn btn-primary m-t-15 waves-effect']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>
@stop

@section('js')
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/admin/pages/users/validate.js"></script>
    <script src="/admin/plugins/tinymce/tinymce.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
@stop
