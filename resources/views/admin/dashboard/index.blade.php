@inject('dateService', 'App\Services\DateService')
@extends('admin.layouts.layouts')

@section('css')
    <!-- Morris Chart Css-->
    <link href="/admin/plugins/morrisjs/morris.css" rel="stylesheet"/>
    <link href="/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <link href="/admin/plugins/waitme/waitMe.css" rel="stylesheet"/>

    <style>
        .select2-container--default .select2-selection--single{
            border: none;
            border-radius: 0;
            height: 34px;
        }
    </style>
@stop

@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <h2>Dashboard</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12">
                                <h2>LỌC THEO</h2>
                            </div>
                        </div>
                    </div>


                    <div class="body">
                        {!! Form::open(['route' => 'admin.dashboard', 'method' => 'GET']) !!}
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group form-float" id="startDate">
                                    <div class="form-line">
                                        {!! Form::text('date_from', \Request::get('date_from', $dateService->getStartOfMonth()), [
                                            'class' => 'form-control'
                                         ])!!}
                                        <label class="form-label">Kết thúc</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-float" id="endDate">
                                    <div class="form-line">
                                        {!! Form::text('date_to', \Request::get('date_to', $dateService->getEndOfMonth()), [
                                            'class' => 'form-control'
                                         ])!!}
                                        <label class="form-label">Kết thúc</label>
                                    </div>
                                </div>
                            </div>
                            @if(\Illuminate\Support\Facades\Gate::allows('isSuperAdmin'))
                            <div class="col-sm-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('user_id', array_replace(['' => '--Thành viên--'], $dropDownUsers), \Request::get('user_id', ''), ['class' => 'form-control show-tick','data-live-search' => 'true', 'id' => 'isActive']) !!}
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-sm-2">
                                <div class="form-line">
                                    <button type="submit" class="btn bg-cyan btn-block btn-lg waves-effect">Áp dụng</button>

                                </div>

                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">attach_money</i>
                    </div>
                    <div class="content">
                        <div class="text">THU NHẬP</div>
                        <div class="number count-to" data-from="0" data-to="{{$income}}" data-speed="15"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">shopping_cart</i>
                    </div>
                    <div class="content">
                        <div class="text">ĐƠN HÀNG</div>
                        <div class="number count-to" data-from="0" data-to="{{$orders}}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="content">
                        <div class="text">KHOÁ HỌC</div>
                        <div class="number count-to" data-from="0" data-to="{{$courses}}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">forum</i>
                    </div>
                    <div class="content">
                        <div class="text">BÌNH LUẬN</div>
                        <div class="number count-to" data-from="0" data-to="{{$comments}}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>

        </div>
        <!-- #END# Widgets -->
        <!-- CPU Usage -->
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12">
                                <h2>THU NHẬP TRONG THÁNG</h2>
                            </div>
                        </div>
                    </div>


                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div id="myChart" class="chart--container">
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- #END# CPU Usage -->
        {{--<div class="row clearfix">--}}
        {{--<!-- Visitors -->--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">--}}
        {{--<div class="card">--}}
        {{--<div class="body bg-pink">--}}
        {{--<div class="sparkline" data-type="line" data-spot-Radius="4"--}}
        {{--data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#fff"--}}
        {{--data-min-Spot-Color="rgb(255,255,255)" data-max-Spot-Color="rgb(255,255,255)"--}}
        {{--data-spot-Color="rgb(255,255,255)"--}}
        {{--data-offset="90" data-width="100%" data-height="92px" data-line-Width="2"--}}
        {{--data-line-Color="rgba(255,255,255,0.7)"--}}
        {{--data-fill-Color="rgba(0, 188, 212, 0)">--}}
        {{--12,10,9,6,5,6,10,5,7,5,12,13,7,12,11--}}
        {{--</div>--}}
        {{--<ul class="dashboard-stat-list">--}}
        {{--<li>--}}
        {{--TODAY--}}
        {{--<span class="pull-right"><b>1 200</b> <small>USERS</small></span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--YESTERDAY--}}
        {{--<span class="pull-right"><b>3 872</b> <small>USERS</small></span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--LAST WEEK--}}
        {{--<span class="pull-right"><b>26 582</b> <small>USERS</small></span>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- #END# Visitors -->--}}
        {{--<!-- Latest Social Trends -->--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">--}}
        {{--<div class="card">--}}
        {{--<div class="body bg-cyan">--}}
        {{--<div class="m-b--35 font-bold">LATEST SOCIAL TRENDS</div>--}}
        {{--<ul class="dashboard-stat-list">--}}
        {{--<li>--}}
        {{--#socialtrends--}}
        {{--<span class="pull-right">--}}
        {{--<i class="material-icons">trending_up</i>--}}
        {{--</span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--#materialdesign--}}
        {{--<span class="pull-right">--}}
        {{--<i class="material-icons">trending_up</i>--}}
        {{--</span>--}}
        {{--</li>--}}
        {{--<li>#adminbsb</li>--}}
        {{--<li>#freeadmintemplate</li>--}}
        {{--<li>#bootstraptemplate</li>--}}
        {{--<li>--}}
        {{--#freehtmltemplate--}}
        {{--<span class="pull-right">--}}
        {{--<i class="material-icons">trending_up</i>--}}
        {{--</span>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- #END# Latest Social Trends -->--}}
        {{--<!-- Answered Tickets -->--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">--}}
        {{--<div class="card">--}}
        {{--<div class="body bg-teal">--}}
        {{--<div class="font-bold m-b--35">ANSWERED TICKETS</div>--}}
        {{--<ul class="dashboard-stat-list">--}}
        {{--<li>--}}
        {{--TODAY--}}
        {{--<span class="pull-right"><b>12</b> <small>TICKETS</small></span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--YESTERDAY--}}
        {{--<span class="pull-right"><b>15</b> <small>TICKETS</small></span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--LAST WEEK--}}
        {{--<span class="pull-right"><b>90</b> <small>TICKETS</small></span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--LAST MONTH--}}
        {{--<span class="pull-right"><b>342</b> <small>TICKETS</small></span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--LAST YEAR--}}
        {{--<span class="pull-right"><b>4 225</b> <small>TICKETS</small></span>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--ALL--}}
        {{--<span class="pull-right"><b>8 752</b> <small>TICKETS</small></span>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- #END# Answered Tickets -->--}}
        {{--</div>--}}

        {{--<div class="row clearfix">--}}
        {{--<!-- Task Info -->--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">--}}
        {{--<div class="card">--}}
        {{--<div class="header">--}}
        {{--<h2>Bình luận mới</h2>--}}

        {{--</div>--}}
        {{--<div class="body">--}}
        {{--<div class="table-responsive">--}}
        {{--<table class="table table-hover dashboard-task-infos">--}}
        {{--<thead>--}}
        {{--<tr>--}}
        {{--<th>#</th>--}}
        {{--<th>Task</th>--}}
        {{--<th>Status</th>--}}
        {{--<th>Manager</th>--}}
        {{--<th>Progress</th>--}}
        {{--</tr>--}}
        {{--</thead>--}}
        {{--<tbody>--}}
        {{--<tr>--}}
        {{--<td>1</td>--}}
        {{--<td>Task A</td>--}}
        {{--<td><span class="label bg-green">Doing</span></td>--}}
        {{--<td>John Doe</td>--}}
        {{--<td>--}}
        {{--<div class="progress">--}}
        {{--<div class="progress-bar bg-green" role="progressbar" aria-valuenow="62"--}}
        {{--aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>--}}
        {{--</div>--}}
        {{--</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
        {{--<td>2</td>--}}
        {{--<td>Task B</td>--}}
        {{--<td><span class="label bg-blue">To Do</span></td>--}}
        {{--<td>John Doe</td>--}}
        {{--<td>--}}
        {{--<div class="progress">--}}
        {{--<div class="progress-bar bg-blue" role="progressbar" aria-valuenow="40"--}}
        {{--aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>--}}
        {{--</div>--}}
        {{--</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
        {{--<td>3</td>--}}
        {{--<td>Task C</td>--}}
        {{--<td><span class="label bg-light-blue">On Hold</span></td>--}}
        {{--<td>John Doe</td>--}}
        {{--<td>--}}
        {{--<div class="progress">--}}
        {{--<div class="progress-bar bg-light-blue" role="progressbar"--}}
        {{--aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"--}}
        {{--style="width: 72%"></div>--}}
        {{--</div>--}}
        {{--</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
        {{--<td>4</td>--}}
        {{--<td>Task D</td>--}}
        {{--<td><span class="label bg-orange">Wait Approvel</span></td>--}}
        {{--<td>John Doe</td>--}}
        {{--<td>--}}
        {{--<div class="progress">--}}
        {{--<div class="progress-bar bg-orange" role="progressbar" aria-valuenow="95"--}}
        {{--aria-valuemin="0" aria-valuemax="100" style="width: 95%"></div>--}}
        {{--</div>--}}
        {{--</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
        {{--<td>5</td>--}}
        {{--<td>Task E</td>--}}
        {{--<td>--}}
        {{--<span class="label bg-red">Suspended</span>--}}
        {{--</td>--}}
        {{--<td>John Doe</td>--}}
        {{--<td>--}}
        {{--<div class="progress">--}}
        {{--<div class="progress-bar bg-red" role="progressbar" aria-valuenow="87"--}}
        {{--aria-valuemin="0" aria-valuemax="100" style="width: 87%"></div>--}}
        {{--</div>--}}
        {{--</td>--}}
        {{--</tr>--}}
        {{--</tbody>--}}
        {{--</table>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--</div>--}}
    </div>
@stop

@section('js')

    <!-- Jquery CountTo Plugin Js -->
    <script src="/admin/plugins/jquery-countto/jquery.countTo.js"></script>

    <script src="/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Sparkline Chart Plugin Js -->
    <script src="/admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>

    <script>
        var labelX = <?= json_encode($chart['label_x']) ?>;
        var labelY = <?= json_encode($chart['label_y']) ?>;
        labelY = labelY.map(function(label) {
            return parseInt(label);
        });
    </script>
    <script src="/admin/pages/dashboard/index.js"></script>
@stop
