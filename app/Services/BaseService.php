<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Log;

class BaseService {
    protected $repository;

    public function find($id, $relations = []) {
        return $this->repository->find($id, $relations);
    }

    public function findAndDelete($id) {

        if (!$instance = $this->find($id)) {
            return false;
        }

        if ($instance instanceof $this->repository->model) {
            $delete = $this->repository->delete($instance);
            if($delete) return $instance;
        }

        return false;
    }

    public function findAndUpdate($id, $data = []) {
        try {
            if (!$instance = $this->find($id)) {
                return false;
            }

            if ($instance instanceof $this->repository->model) {
                $instance->fill($data)->save();
            }

            return $instance;
        } catch (Exception $exception) {
            Log::error('Something went wrong when delete ' . __METHOD__ . ' with : ' . $exception->getMessage());
            return false;
        }
    }

    public function insert($data) {
        return $this->repository->insert($data);
    }

    public function pluckAll() {
        return $this->repository->pluckAll();
    }

    public function countTo($wheres = []) {
        return $this->repository->countTo($wheres);
    }

    public function first() {
        return $this->repository->first();
    }
}
