<?php

namespace App\Services;

use Exception;
use App\Repositories\User\UserRepositoryInterface;

class UserService extends BaseService {

    protected $courseService;

    public function __construct(UserRepositoryInterface $userRepository, CourseService $courseService) {
        $this->repository = $userRepository;
        $this->courseService = $courseService;
    }

    public function getAsJson($filters) {
        return $this->repository->getAsJson($filters);
    }

    public function findAndUpdate($id, $data = []) {
        try {
            if (!$instance = $this->find($id)) {
                return false;
            }
            $data['password'] = $data['password'] ? bcrypt($data['password']) : $instance->password;

            if ($instance instanceof $this->repository->model) {
                $instance->fill($data)->save();
            }

            return $instance;
        } catch (Exception $exception) {
            \Log::error('Something went wrong when update ' . __METHOD__ . ' with : ' . $exception->getMessage());
            return false;
        }
    }

    public function pluckAdmin() {
        return $this->repository->pluckAdmin();
    }

}
