<?php

namespace App\Jobs;

use App\Models\Order;
use App\Services\SmsService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsJob {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $order;
    protected $smsService;

    /**
     * Create a new job instance.
     * @param $order
     * @param $smsService
     *
     * @return void
     */
    public function __construct(Order $order) {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        if (!empty($this->order->user->phone_number)) {
            (new SmsService($this->order->user->phone_number, 'Đơn hàng mã ' . $this->order->id . ' đã xác nhận. Vui lòng kiểm tra email'))->send();
        }
    }
}
