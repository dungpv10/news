<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

use Prophecy\Exception\Doubler\ClassNotFoundException;
use Yajra\DataTables\DataTables;
use DB;

abstract class BaseRepository {
    public $model;

    protected $dataTable;

    public function __construct(DataTables $dataTable) {

        $model = $this->model();

        if (!class_exists($model)) {
            throw new ClassNotFoundException('Class not found', $model);
        }
        $this->model = new $model();
        $this->dataTable = $dataTable;

    }

    public function find($id, $relations = []) {

        return empty($relations)
            ? $this->model->find($id)
            : $this->model->with($relations)->find($id);
    }

    public function delete(Model $instance) {

        return $instance->delete();

    }

    public function insert($data) {
        return $this->model->create($data);
    }

    public function getAsJson($filters, $relations = []) {
        try {
            $query = $this->model->whereRaw('1=1');
            if($relations) {
                $query->with($relations);
            }
            if (empty($filters)) {
                return $this->dataTable->of($query)->toJson();
            }

            foreach ($filters as $attribute => $value) {

                if (!empty($value)) {
                    $query->where($attribute, 'LIKE', '%' . $value . '%');
                }
            }

            return $this->dataTable->of($query)->toJson();
        } catch (\Exception $exception) {
            Log::error('Something went wrong with ' . __METHOD__ . ' with message : ' . $exception->getMessage());
            return false;
        }

    }


    public function pluckAll($label = 'name') {
        return $this->model->pluck($label, 'id')->toArray();
    }

    protected abstract function model();

    public function countTo($wheres = []) {
        if(empty($wheres)) {
            return $this->model->count();
        }

        return $this->model->where($wheres)->count();
    }

    public function first() {
        return $this->model->first();
    }

    protected function raw($strQuery) {
        return DB::raw($strQuery);
    }

    protected function filterByDate($filters, $builder) {
        if(isset($filters['date_from'])) {
            $builder->where('created_at', '>=', $filters['date_from']);
        }
        if(isset($filters['date_to'])) {
            $builder->where('created_at', '<=', $filters['date_to']);
        }

        return $builder;
    }
}
