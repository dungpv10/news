<?php


namespace App\Factories\GetIncome;


use App\Services\DateService;

abstract class GetIncomeAbstract {

    public abstract function getIncome($filters = []);

    public abstract function getTotalOrders($filters);

    public function getTotalCourses($filters) {
        return 0;
    }

    public abstract function getRevenueForCharts($filters);


    protected function groupByYear($data) {
        return $this->groupBy($data, 'year');
    }


    private function groupBy($data, $column) {
        if (empty($data)) return [];

        return collect($data)->groupBy('by_' . $column);
    }

    protected function groupByMonth($data) {
        return $this->groupBy($data, 'month');
    }

    protected function collectDataForCharts($data) {
        $dataAsGroup = $this->groupByYear($data)->toArray();

        $result = [];
        foreach ($dataAsGroup as $year => $months) {

            $dataOfMonths = $this->groupByMonth($months)->toArray();

            foreach ($dataOfMonths as $month => $dataOfMonth) {
                $result[$month . '/' . $year] = head($dataOfMonth)['total_price'];
            }
        }

        return $result;
    }

    protected function getLabelXForChart($data) {
        return array_keys($data);
    }

    protected function getLabelYForChart($data) {
        return array_values($data);
    }

    protected function getDefaultDateFilter($filters) {
        if (!isset($filters['date_from']) && !isset($filters['date_to'])) {
            $dateService = new DateService();
            $filters['date_from'] = $dateService->getStartOfMonth();
            $filters['date_to'] = $dateService->getEndOfMonth();
        }

        $filters['date_from'] = date('Y-m-d', strtotime($filters['date_from']));
        $filters['date_to'] = date('Y-m-d', strtotime($filters['date_to']));

        return $filters;
    }

    protected function getLabelForChart($data) {
        $groupBy = $this->collectDataForCharts($data);

        return [
            'label_x' => $this->getLabelXForChart($groupBy),
            'label_y' => $this->getLabelYForChart($groupBy)
        ];

    }
}
