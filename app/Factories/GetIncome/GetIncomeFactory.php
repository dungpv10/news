<?php
/**
 * Created by PhpStorm.
 * User: dungpv
 * Date: 9/9/19
 * Time: 22:01
 */

namespace App\Factories\GetIncome;


use App\Factories\GetIncome\Classes\Seller;
use App\Factories\GetIncome\Classes\SuperAdmin;
use App\Factories\GetIncome\Classes\Teacher;

class GetIncomeFactory {
    public function make($userType) {
        switch ($userType){
            case ADMIN:
                return new SuperAdmin();
                break;
            case TEACHER:
                return new Teacher();
                break;
            case SALE:
                return new Seller();
            default:
                return;
        }
    }
}
