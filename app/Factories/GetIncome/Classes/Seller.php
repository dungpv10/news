<?php

namespace App\Factories\GetIncome\Classes;


use App\Factories\GetIncome\GetIncomeAbstract;

class Seller extends GetIncomeAbstract {
    public function getIncome($userId = null) {
        return auth()->user()->userIncomes->sum('income');
    }

    public function getTotalOrders($userId = null) {
        // TODO: Implement getTotalOrders() method.
    }
}
