<?php

namespace App\Factories\GetIncome\Classes;


use App\Factories\GetIncome\GetIncomeAbstract;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Log;

class Teacher extends GetIncomeAbstract {

    protected $orderRepository;
    protected $userRepository;
    protected $courseRepository;

    public function __construct() {
        try {
            $this->orderRepository = app()->make(OrderRepositoryInterface::class);
            $this->userRepository = app()->make(UserRepositoryInterface::class);
            $this->userRepository = app()->make(UserRepositoryInterface::class);
            $this->courseRepository = app()->make(CourseRepositoryInterface::class);

        } catch (\Exception $exception) {
            Log::error('Something went wrong when initialize OrderRepositoryInterface');
        }

    }

    public function getIncome($filters = []) {
        $filters = $this->getDefaultDateFilter($filters);

        return $this->userRepository->getIncomeByUserId($filters);
    }

    public function getTotalOrders($filters) {
        $filters = $this->getDefaultDateFilter($filters);

        return $this->userRepository->getTotalOrdersByUserId($filters);
    }

    public function getTotalCourses($filters) {
        $filters = $this->getDefaultDateFilter($filters);

        return $this->courseRepository->getTotalCoursesByUserId($filters);
    }

    public function getRevenueForCharts($filters) {
        $filters = $this->getDefaultDateFilter($filters);

        $data = $this->userRepository->getRevenueByUserId($filters);

        return $this->getLabelForChart($data);
    }
}
