<?php

namespace App\Factories\GetIncome\Classes;


use App\Factories\GetIncome\GetIncomeAbstract;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Log;

class SuperAdmin extends GetIncomeAbstract {
    protected $orderRepository;
    protected $userRepository;
    protected $courseRepository;

    public function __construct() {
        try {
            $this->orderRepository = app()->make(OrderRepositoryInterface::class);
            $this->userRepository = app()->make(UserRepositoryInterface::class);
            $this->userRepository = app()->make(UserRepositoryInterface::class);
            $this->courseRepository = app()->make(CourseRepositoryInterface::class);

        } catch (\Exception $exception) {
            Log::error('Something went wrong when initialize OrderRepositoryInterface');
        }

    }

    public function getIncome($filters = []) {
        $filters = $this->getDefaultDateFilter($filters);

        if (!isset($filters['user_id'])) {
            return $this->orderRepository->getAllIncome($filters);
        }

        return $this->userRepository->getIncomeByUserId($filters);
    }

    public function getTotalOrders($filters) {
        $filters = $this->getDefaultDateFilter($filters);

        if (!isset($filters['user_id'])) {
            return $this->orderRepository->getTotalOrders($filters);
        }

        return $this->userRepository->getTotalOrdersByUserId($filters);
    }

    public function getTotalCourses($filters) {
        $filters = $this->getDefaultDateFilter($filters);

        if (!isset($filters['user_id'])) {
            return $this->courseRepository->getTotalCourses($filters);
        }

        return $this->courseRepository->getTotalCoursesByUserId($filters);
    }

    public function getRevenueForCharts($filters) {
        $filters = $this->getDefaultDateFilter($filters);

        $data = !isset($filters['user_id'])
            ? $this->orderRepository->getAllRevenue($filters)
            : $this->userRepository->getRevenueByUserId($filters);

        return $this->getLabelForChart($data);
    }

}
