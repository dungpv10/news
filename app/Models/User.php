<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $appends = ['role_name', 'active_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token',];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['email_verified_at' => 'datetime',];

    public function getRoleNameAttribute() {
        $roles = config('role');
        $userRoleId = $this->getAttribute('role_id');

        return in_array($userRoleId, array_keys($roles)) ? $roles[$userRoleId] : $roles[NORMAL];
    }

    public function getActiveNameAttribute() {
        $statuses = config('user.status');
        $userActive = $this->getAttribute('is_active');

        return in_array($userActive, array_keys($statuses)) ? $statuses[$userActive] : $statuses[IS_INACTIVE];
    }

    // Scope
    public function scopeIsAdmin($query) {
        return $query->where('role_id', ADMIN);
    }

    // End scope

    private function isRole($roleIds = []) {
        $user = auth()->user();

        return in_array($user->role_id, $roleIds);
    }

    public function isSuperAdmin() {
        return $this->isRole([ADMIN]);
    }

    public function isTeacher() {
        return $this->isRole([TEACHER]);
    }

    public function isSeller() {
        return $this->isRole([SALE]);
    }

    public function isManager() {
        return $this->isRole([ADMIN, TEACHER, SALE]);
    }

    public function isNormal() {
        return $this->isRole([NORMAL]);
    }
}
