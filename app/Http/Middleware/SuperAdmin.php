<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param array $roles
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Gate::allows('isSuperAdmin')) {
            abort(403);
        }
        return $next($request);
    }
}
