<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\JsonResponseRequest;


class CourseRequest extends JsonResponseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:5|max:255',
            'price' => 'numeric|nullable',
            'subject_id' => 'required',
            'class_name.*' => 'required|min:2|max:255',
            'class_media.*' => 'required|mimes:flv,mp4,avi,m3u8,mov,wmv,avi',
            'thumbnail' => 'required|mimes:jpg,jpeg,png,gif'
        ];
        return $rules;
    }


}
