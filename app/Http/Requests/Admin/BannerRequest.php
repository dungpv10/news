<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'url_path' => 'required|url',
            'order' => 'required|integer',
            'banner' => 'required|mimes:jpg,jpeg,png,gif'
        ];

        $routeName = $this->route()->getName();

        if($routeName === 'banners.update') {
            $rules = array_replace($rules, [
                'banner' => 'nullable|mimes:jpg,jpeg,png,gif'
            ]);
        }

        return $rules;
    }
}
