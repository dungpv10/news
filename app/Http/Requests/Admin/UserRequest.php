<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:2|max:255',
            'phone_number' => 'required|min:10|max:11',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|min:5|max:255|confirmed',
            'password_confirmation' => 'required|min:5|max:255',
            'description' => 'required|max: 1000',
            'role_id' => 'required'
        ];
        $routeName = $this->route()->getName();

        if($routeName == 'users.update') {
            $rules = array_merge($rules, [
                'email' => 'required|email|min:5|max:255|unique:users,email,' . $this->route('user'),
                'password' => 'nullable|min:5|max:255|confirmed',
                'password_confirmation' => 'nullable|min:5|max:255',
            ]);
        }

        return $rules;
    }
}
