<?php

namespace App\Http\Controllers;

use App\Services\ThematicService;
use Illuminate\Http\Request;

class ThematicController extends Controller
{
    protected $thematicService;

    public function __construct(ThematicService $thematicService) {
        $this->thematicService = $thematicService;
    }

    public function getSubjects(Request $request) {
        $subjects = $this->thematicService->getSubjects($request->get('thematic_id'));

        return response()->json([
            'code' => 200,
            'subjects' => $subjects
        ]);
    }
}
