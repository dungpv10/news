<?php

namespace App\Http\Controllers\Admin;

use App\Services\SettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class SettingController extends Controller {
    protected $service;

    public function __construct(SettingService $service) {
        $this->service = $service;
    }

    public function index(Request $request) {
        $setting = $this->service->first();

        return view('admin.settings.index', compact('setting'));
    }

    public function store(Request $request) {
        try {
            $setting = $this->service->storeOrUpdate($request);

            return response()->json(['code' => 200, 'message' => 'Change setting successfully', 'setting' => $setting]);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when create|update setting ' . $exception->getMessage());

            return response()->json(['code' => 500, 'message' => 'Internal server'], 500);
        }

    }
}
