<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller {

    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request) {
        $request->merge(['password' => bcrypt($request->get('password'))]);
        $user = $this->userService->insert($request->except('_token', 'password_confirmation'));
        if ($user) {
            return redirect()->route('users.index')->with('success', 'Tạo mới thành viên thành công ');
        }

        return redirect()->back()->with('error', 'Xảy ra lỗi trong quá trình thao tác, vui lòng thử lại');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = $this->userService->find($id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id) {
        $user = $this->userService->findAndUpdate($id, $request->except('_token', 'password_confirmation'));

        if (!$user) {
            return redirect()->back()->with('success', 'Cập nhật thông tin thành công');
        }
        return redirect()->route('users.index')->with('success', 'Cập nhật thông tin thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $this->userService->findAndDelete($id);

        return response()->json(['code' => 200, 'msg' => 'Đã xoá']);
    }

    public function asJson(Request $request) {

        return $this->userService->getAsJson($request->only('role_id', 'is_active', 'can_upload'));
    }
}
