<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CourseRequest;
use App\Models\Setting;
use App\Services\CourseService;
use App\Services\LevelService;
use App\Services\SubjectService;
use App\Services\ThematicService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CourseController extends Controller {
    protected $courseService;
    protected $thematicService;
    protected $subjectService;

    public function __construct(CourseService $courseService, ThematicService $thematicService, SubjectService $subjectService) {
        $this->courseService = $courseService;
        $this->thematicService = $thematicService;
        $this->subjectService = $subjectService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.courses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $thematics = $this->thematicService->pluckAll();

        return view('admin.courses.create', compact('thematics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CourseRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request) {
        try {
            $this->courseService->uploadAndInsert($request);
            return response()->json(['code' => 200, 'message' => 'Create course successfully'], 200);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when store course ' . $exception->getMessage());
            return response()->json(['code' => 500, 'message' => 'Internal server'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        try {
            $course = $this->courseService->find($id, ['lessons']);
            $thematics = $this->thematicService->pluckAll();

            return view('admin.courses.edit', compact('course', 'thematics'));
        } catch (\Exception $exception) {
            Log::error('Something went wrong when store course ' . $exception->getMessage());
            return response()->json(['code' => 500, 'message' => 'Internal server'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateCourse(Request $request, $id) {
        try {
            $this->courseService->findAndUpdateCourse($id, $request);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when store course ' . $exception->getMessage());
            return response()->json(['code' => 500, 'message' => 'Internal server'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $this->courseService->findAndDelete($id);
            return response()->json(['code' => 200, 'message' => 'Delete successfully'], 200);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when delete course');
            return response()->json(['code' => 500, 'message' => 'Delete successfully'], 500);
        }

    }

    public function asJson(Request $request) {
        return $this->courseService->getAsJson($request->only('is_reviewed'));
    }

}
