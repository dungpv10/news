<?php

namespace App\Http\Controllers\Admin;

use App\Events\OrderHasBeenVerified;
use App\Jobs\SendMailJob;

use App\Jobs\SendSmsJob;
use App\Services\CourseService;
use App\Services\OrderService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller {
    protected $orderService;
    protected $userService;
    protected $courseService;

    public function __construct(OrderService $orderService, UserService $userService, CourseService $courseService) {
        $this->orderService = $orderService;
        $this->userService = $userService;
        $this->courseService = $courseService;
    }

    public function index() {
        $admin = $this->userService->pluckAdmin();

        return view('admin.orders.index', compact('admin'));
    }

    public function show($id) {
        $order = $this->orderService->find($id);

        $courses = $this->courseService->getCoursesInIds($order->course_ids_as_array);

        return response()->json(['code' => 200, 'message' => 'Get courses successfully', 'data' => $courses]);
    }

    public function update(Request $request, $id) {

        $request->merge(['admin_id' => auth()->user()->id]);
        try {
            DB::transaction(function() use($id, $request){
                $order = $this->orderService->findAndUpdate($id, $request->only('status', 'admin_id'));

                if ($request->get('status') == PAYMENT_SUCCESS) {
                    $this->userService->insertUserIncome($order);

                    event(new OrderHasBeenVerified($order));
                }

            });
            return response()->json(['code' => 200, 'message' => 'update successfully']);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when update order '. $exception->getMessage());
            return response()->json(['code' => 500, 'message' => 'Internal server error'], 500);
        }

    }

    public function asJson(Request $request) {
        return $this->orderService->getAsJson($request->only('status', 'admin_id'));
    }
}
