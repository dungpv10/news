<?php

namespace App\Http\Controllers\Admin;

use App\Factories\GetIncome\GetIncomeFactory;
use App\Services\CommentService;
use App\Services\CourseService;
use App\Services\DocumentService;
use App\Services\OrderService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class DashboardController extends Controller {
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function index(Request $request) {

    }
}
