<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider {
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [// 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot() {
        $this->registerPolicies();

        Gate::define('isSuperAdmin', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('isTeacher', function (User $user) {
            return $user->isTeacher();
        });

        Gate::define('isSeller', function (User $user) {
            return $user->isSeller();
        });

        Gate::define('isAdmin', function(User $user) {
            return $user->isManager();
        });

        Gate::define('isNormal', function (User $user) {
            return $user->isNormal();
        });

    }

}
