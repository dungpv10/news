<?php

namespace App\Providers;

use App\Models\Course;
use App\Models\Document;
use App\Models\Level;
use App\Models\User;
use App\Observers\CourseObserver;
use App\Observers\DocumentObserver;
use App\Observers\LevelObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
    }
}
