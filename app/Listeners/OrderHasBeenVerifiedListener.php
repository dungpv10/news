<?php

namespace App\Listeners;

use App\Events\OrderHasBeenVerified;
use App\Jobs\SendMailJob;
use App\Jobs\SendSmsJob;
use App\Models\Order;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderHasBeenVerifiedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderHasBeenVerified  $event
     * @return void
     */
    public function handle(OrderHasBeenVerified $event)
    {
        if($event->order instanceof Order){
            SendMailJob::dispatch($event->order);
            SendSmsJob::dispatch($event->order);
        }

    }
}
